---
marp: true
theme: uncover
---

<style>
section {
    text-align: left;
    font-size: 28px;
}
ul {
    margin-left: 0px;
}
section.center p, section.center ul, section.center li,
section.center h1,  section.center h2, section.center h3,
section.center h4,  section.center h5, section.center h6  {
    text-align: center;
}
section h1 {
    font-size: 40px;
}
section::after {
  font-weight: bold;
}
section.cover header{
    text-align: right;
    opacity: 0.1;
}
</style>

<!-- _class: center invert cover -->

<style scoped>
    section h1 {
        font-size: 55px;
    }
    footer {
        text-align: right;
    }
</style>

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->

<!-- _footer: Powered by [Marp](https://yhatt.github.io/marp/) & [PlantUML](https://github.com/plantuml) -->

# Marp PlantUML Template

Marp
PlantUML
embedme

---

PlantUML & Embedme Example:

<!-- embedme  src/main/plantuml/uml.puml -->

```puml
@startuml
skinparam monochrome reverse
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
@enduml
```

![bg right fit](resources/uml.svg)




---

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->
<!-- paginate: false -->
<!-- _class: center invert cover -->
